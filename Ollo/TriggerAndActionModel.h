//
//  TriggerAndActionModel.h
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TriggerAndActionModel : NSObject

@property(nonatomic,retain) NSString *select_icon;
@property(nonatomic,retain) NSString *config_icon;
@property(nonatomic,retain) NSArray  *conditionsArray;
@property(nonatomic,retain) NSArray  *actionArray;
@property(nonatomic,retain) NSDictionary *device;

- (id)initWithDeviceType:(NSString *)triggerAndActionKey;

@end
