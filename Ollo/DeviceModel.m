//
//  DeviceModel.m
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "DeviceModel.h"

@implementation DeviceModel
@synthesize allKeyArray;
@synthesize allObjectArray;
@synthesize deviceType;
@synthesize defaultName;


-(id)initDeviceModel
{
    NSUserDefaults *deviceInfo = [NSUserDefaults standardUserDefaults];
    self = [super init];
    if(self)
    {
        self.allKeyArray = [deviceInfo objectForKey:@"deviceAllKeys"];
        self.allObjectArray = [deviceInfo objectForKey:@"deviceAllObjects"];
    }
    return self;
}

-(void)initValue:(NSString *)inputDeviceType
{
    NSInteger tag = [self.allKeyArray indexOfObject:inputDeviceType];
    if (self) {
        self.deviceType = [[self.allObjectArray objectAtIndex:tag] objectForKey:@"device_type"];
        self.defaultName = [[self.allObjectArray objectAtIndex:tag] objectForKey:@"default_name"];
    }
}


@end
