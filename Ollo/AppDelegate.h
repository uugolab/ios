//
//  AppDelegate.h
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,retain) UINavigationController *nav;




@end

