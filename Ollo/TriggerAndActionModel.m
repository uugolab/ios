//
//  TriggerAndActionModel.m
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "TriggerAndActionModel.h"

@implementation TriggerAndActionModel

@synthesize select_icon;
@synthesize config_icon;
@synthesize conditionsArray;
@synthesize actionArray;
@synthesize device;

- (id)initWithDeviceType:(NSString *)triggerAndActionKey
{
    NSUserDefaults *ruleSampleInfo = [NSUserDefaults standardUserDefaults];
    
    //Get triggerAndAction Data
    NSDictionary *triggerAndActionDic = [ruleSampleInfo objectForKey:@"ruleSampleDic"];
    
    self = [super init];
    if (self)
    {
        self.select_icon        = [[triggerAndActionDic objectForKey:triggerAndActionKey] objectForKey:@"select_icon"];
        self.config_icon        = [[triggerAndActionDic objectForKey:triggerAndActionKey] objectForKey:@"config_icon"];
        self.conditionsArray    = [[triggerAndActionDic objectForKey:triggerAndActionKey] objectForKey:@"conditions"];
        self.actionArray        = [[triggerAndActionDic objectForKey:triggerAndActionKey] objectForKey:@"actions"];
    }
    return self;
}

@end
