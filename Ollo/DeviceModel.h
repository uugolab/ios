//
//  DeviceModel.h
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceModel : NSObject
@property(nonatomic,retain) NSArray *allKeyArray;
@property(nonatomic,retain) NSArray *allObjectArray;
@property(nonatomic,retain) NSString *deviceType;
@property(nonatomic,retain) NSString *defaultName;
-(id)initDeviceModel;
-(void)initValue:(NSString *)inputDeviceType;
@end
