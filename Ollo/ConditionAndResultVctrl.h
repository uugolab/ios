//
//  ConditionAndResultVctrl.h
//  Ollo
//
//  Created by Bosim on 14-9-25.
//  Copyright (c) 2014年 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
@class TriggerAndActionModel;

@interface ConditionAndResultVctrl : UIViewController<UITableViewDataSource,UITableViewDelegate,WYPopoverControllerDelegate>

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(TriggerAndActionModel*)data;


@end
