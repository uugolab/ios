//
//  LoginViewController.h
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"
#import "TableDataHelper.h"
#import "InterfaceDelegate.h"

@interface LoginViewController : UIViewController<InterfaceDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *signupBtn;
@property (weak, nonatomic) IBOutlet UIButton *signinBtn;
@property (weak, nonatomic) IBOutlet UITextField *usernameTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (nonatomic,retain) TableDataHelper *tableDataHelper;

-(void)fillUsernameAndPassword:(NSString *)username;
@end
