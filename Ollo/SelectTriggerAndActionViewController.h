//
//  SelectTriggerAndActionViewController.h
//  Ollo
//
//  Created by Allan.Chan on 9/15/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableDataHelper.h"
#import "InterfaceDelegate.h"
#import "WYPopoverController.h"

@interface SelectTriggerAndActionViewController : UIViewController<InterfaceDelegate,WYPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
    WYPopoverController* popoverController;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil selectType:(BOOL)result;

@property (weak, nonatomic) IBOutlet UIScrollView *triggerAndActionScrollView;
@property (weak, nonatomic) IBOutlet UIView *triggerAndActionView;
@property (nonatomic,retain) TableDataHelper *tableDataHelper;
@property (nonatomic,retain) NSString *triggerAndActionTagString;
@property (nonatomic,retain) UITableViewController *showConditionTableViewController;
@end
