//
//  ConfigWebVctrl.h
//  Ollo
//
//  Created by Bosim on 14-9-26.
//  Copyright (c) 2014年 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TriggerAndActionModel;

@interface ConfigWebVctrl : UIViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(TriggerAndActionModel*)data index:(int)index;

@property(nonatomic,retain)IBOutlet UIWebView *configWebView;

@end
