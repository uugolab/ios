//
//  UIViewController+Helper.m
//  Lsgo
//
//  Created by Allan.Chan on 8/27/14.
//  Copyright (c) 2014 Allan. All rights reserved.
//

#import "UIViewController+Helper.h"
#import "LoadingView.h"
@implementation UIViewController (Helper)

-(void)showLoading
{
    [[LoadingView shareLoadingView] setLoadingViewAlpha:1.0];
    [[LoadingView shareLoadingView] shareLoadingViewFrame];
    [[LoadingView shareLoadingView] setMessage:@"请稍后.."];
    [self.view addSubview:[LoadingView shareLoadingView]];
}

-(void)removeLoading
{
    [UIView animateWithDuration:0.4 animations:^{
        [[LoadingView shareLoadingView] setLoadingViewAlpha:0.0];
    } completion:^(BOOL finished) {
        [[LoadingView shareLoadingView] removeAll];
    }];
}
@end
