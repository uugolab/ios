//
//  UITableView+TableHelper.h
//  JueWeiShop
//
//  Created by Allan.Chan on 13-2-17.
//
//

#import <UIKit/UIKit.h>

@interface UIScrollView (UIScrollViewHelper)
/*
 |  load More
 */
-(void)loadMore:(UIButton *)loadMoreBtn andActive:(UIActivityIndicatorView *)loadMoreActivity;
-(void)resetLoadMorePosition;
-(void)removeLoadMoreView;

/*
 |  refresh
 */
-(void)refreshIterfaceInit;
-(void)changeRefreshViewLoadingMessage:(CGFloat)dropHeight;
-(void)changeRefreshViewLoadingMessageWithoutParameters;
-(void)refreshingData;
-(void)stopRefreshingData;
@end
