//
//  MainCellTableViewCell.h
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RuleModel.h"
#import "DeviceModel.h"
#import "TableDataHelper.h"
#import "InterfaceDelegate.h"
@interface MainCellTableViewCell : UITableViewCell<InterfaceDelegate>
@property (weak, nonatomic) IBOutlet UILabel *ruleNameLab;
@property (weak, nonatomic) IBOutlet UISwitch *ruleSwitch;
@property (weak, nonatomic) IBOutlet UIView *preconditionView;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (nonatomic,retain) TableDataHelper *tableDataHelper;
-(void)setupCell:(RuleModel *)model;
@end
