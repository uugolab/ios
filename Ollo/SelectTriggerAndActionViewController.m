//
//  SelectTriggerAndActionViewController.m
//  Ollo
//
//  Created by Allan.Chan on 9/15/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "KeychainItemWrapper.h"
#import "UIImageView+AFNetworking.h"
#import "UIViewController+Helper.h"
#import "SelectTriggerAndActionViewController.h"
#import "TriggerAndActionModel.h"
#import "DeviceModel.h"
#import "AFNetworking.h"
#import "UIButton+AFNetworking.h"
#import "ConditionAndResultVctrl.h"


#define kDeviceButtonTag 200

@interface SelectTriggerAndActionViewController ()
{
    BOOL isResult;
}
@property (nonatomic,retain) NSMutableArray *triggerAndActionDataArray;
@end

@implementation SelectTriggerAndActionViewController
@synthesize tableDataHelper;
@synthesize triggerAndActionDataArray;
@synthesize triggerAndActionTagString;
@synthesize showConditionTableViewController;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil selectType:(BOOL)result
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        isResult = result;
    }
    return self;
}

- (void)viewDidLoad
{
    [self initView];
    [super viewDidLoad];
}
-(void)initView
{
    triggerAndActionDataArray = [[NSMutableArray alloc]init];
    
    self.tableDataHelper = [[TableDataHelper alloc] init];
    [self.tableDataHelper setInterfaceDelegate:self];

    NSString *deviceListURLString = [[NSString alloc] initWithFormat:@"%@%@%@",DOMAINNAME,APIVERSION,DEVICELISTURL];
    
    KeychainItemWrapper *accountAccessToken = [[KeychainItemWrapper alloc] initWithIdentifier:@"AccountAccessToken" accessGroup:nil];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
    [postDic setObject:[accountAccessToken objectForKey:(id)CFBridgingRelease(kSecAttrAccount)] forKey:@"access_token"];
    
    [self.tableDataHelper  requestByAsync:deviceListURLString andParameter:postDic andMethod:@"GET" andTag:0 andPassParameter:nil];
    [self showLoading];
}

-(void)uploadDataReturnDic:(NSDictionary *)returnDic andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    [self removeLoading];
    
    /*
     | 获取Devices List成功
     */
    if ([[returnDic objectForKey:@"result"] intValue] == 1)
    {
        [self initTriggerAndActionScrollView:[returnDic objectForKey:@"data"]];
    }
}

-(void)uploadDataFail:(NSDictionary *)returnDic andError:(NSError *)error andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    [self removeLoading];
}

/**
 *  初始化Trigger/Action 的选项
 *
 *  @param triggerAndActionData 所有的Devices
 */
-(void)initTriggerAndActionScrollView:(NSDictionary *)triggerAndActionData
{
    DeviceModel *deviceModel = [[DeviceModel alloc] initDeviceModel];
    
    int totalButton = 0;
    
    for (int i = 0; i < triggerAndActionData.allKeys.count; i++)
    {
        [deviceModel initValue:[[triggerAndActionData allKeys] objectAtIndex:i]];
        
        TriggerAndActionModel *triggerAndActionModel = [[TriggerAndActionModel alloc] initWithDeviceType:deviceModel.deviceType];

        if (triggerAndActionModel.conditionsArray.count > 0)
        {
            [self createTriggerAndActionItem:totalButton target:triggerAndActionModel];
            totalButton++;
        }
        
    }
    self.triggerAndActionScrollView.contentSize = CGSizeMake(totalButton * 65,self.triggerAndActionView.frame.size.height);
}

-(void)createTriggerAndActionItem:(int)index target:(TriggerAndActionModel*)mode
{
    UIButton *triggerBtn = [[UIButton  alloc] init];
    
    triggerBtn.frame = CGRectMake(60*index + 5, 0, 60, 60);
    [triggerBtn setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:[[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,mode.config_icon]] placeholderImage:[UIImage imageNamed:@"default.png"]];
    [triggerBtn addTarget:self action:@selector(showPopShowUp:) forControlEvents:UIControlEventTouchUpInside];
    [self.triggerAndActionScrollView addSubview:triggerBtn];
    
    triggerBtn.tag = kDeviceButtonTag + index;
    
    [triggerAndActionDataArray addObject:mode];
}

-(void)setTriggerAndActionViewCenter:(NSInteger)triggerCount
{
    float weight;
    float originX;
    switch (triggerCount) {
        case 1:
            weight = 65;
            originX = self.view.center.x-(weight/2);
            break;
        case 2:
            weight = 65*2;
            originX = self.view.center.x-(weight/2);
            break;
        case 3:
            weight = 65*3;
            originX = self.view.center.x-(weight/2);
            break;
        case 4:
            weight = 65*4;
            weight = 65;
            originX = self.view.center.x-(weight/2);
            break;
        case 5:
            weight = 65*5;
            weight = 65;
            originX = self.view.center.x-(weight/2);
            break;
        default:
            weight = 65*6;
            weight = 65;
            originX = self.view.center.x-(weight/2);
            break;
    }
    self.triggerAndActionView.frame = CGRectMake(originX, 0, weight, 70);
}

-(IBAction)showPopShowUp:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    TriggerAndActionModel *dataModel = [triggerAndActionDataArray objectAtIndex:btn.tag - kDeviceButtonTag];
    
    //ConditionAndResultVctrl *vCtrl = [[ConditionAndResultVctrl alloc]initWithNibName:@"ConditionAndResultVctrl" bundle:nil data:dataModel];

    ConditionAndResultVctrl *conditionAndResultVC = [[ConditionAndResultVctrl alloc] initWithNibName:@"ConditionAndResultVctrl" bundle:nil data:dataModel];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:conditionAndResultVC];
    popoverController.passthroughViews = @[btn];
    popoverController.popoverLayoutMargins = UIEdgeInsetsMake(100, 10, 10, 10);

    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectMake(0, 110, 0, 0) inView:self.view permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
